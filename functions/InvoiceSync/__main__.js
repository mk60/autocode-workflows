const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});
const { AxiosLightHouse } = require('axios-lighthouse');
const economic = require('../../helpers/economicHelper');
const tg = require('../../helpers/tgHelper');

/**
* @param {number} object_id TG webhook payload
* @param {string} key Lighthouse record key
* @param {number} refId Lighthouse reference Id(Pipedrive product Id)
* @param {object} optional Lighthouse optional object of record
* @param {boolean} isLightHouse is triggered from lighthouse
* @returns {object} result
*/

module.exports = async (object_id = null, key = null, refId = null, optional = {}, isLightHouse = false) => {
  console.log('<----Starting workflow---->', object_id, refId);
  const axios = new AxiosLightHouse({ 
    lightHouseKey: '9KSu0ralWD27AZ8gVS6d',
    entity: 'Invoices Sync', 
    source: 'Tradegecko', 
    issue: 'TradeGecko' 
  });
  
  try {
    if (!isLightHouse) { // trigger from original TG webhook

      if (!object_id) throw new Error('Invoice payload is missing');

      const { key: recKey, optional: opt = {} } = await axios.createLighthouseEntry({ refId: object_id });
      optional = opt;
      key = recKey;
      axios.setRecordKey(key);
    } else { // trigger from lighthouse

      if (!refId) throw new Error('Lighthouse Reference Id is missing');
      if (!key) throw new Error('Lighthouse record key is missing');

      axios.setRecordKey(key);
    }

    const id = object_id ? object_id : refId;

    // get TG invoice by Id
    const invoice = await tg.getInvoice(id, axios);
    await axios.updateLighthouseEntry({ status: 'ongoing', issue: 'Invoice found' });
    console.log('<---Invoice found--->');

    if (!invoice.invoice.company_id) throw new Error('Company id is missing');
    if (!invoice.invoice.billing_address_id && !invoice.invoice.shipping_address_id) throw new Error('Billing & Shipping Address is missing');
    if (!invoice.invoice_line_items) throw new Error('lineitems are missing');

    const companyTG = await tg.getCompany(invoice.invoice.company_id, axios);
    console.log('<---Company found--->');
    if (companyTG.company_type === 'business') {
      const addresses = await tg.getAddress(`${invoice.invoice.billing_address_id}&ids[]=${invoice.invoice.shipping_address_id}`, axios);
      console.log('<---Address found--->');
      await axios.updateLighthouseEntry({ status: 'ongoing', issue: 'Company & Address found' });
  
      let lineItems = [];
      invoice.invoice_line_items.forEach((lineItem) => {
        lineItems.push(lineItem["order_line_item_id"]);
      });
    
      const productDetails = await tg.getOrderLineItems(lineItems.join('&ids[]='), axios);
      const currency = await tg.getCurrency(invoice.invoice.currency_id, axios);
      await axios.updateLighthouseEntry({ status: 'ongoing', issue: 'OrderlineItems & currency found' });
      console.log('<---OrderlineItems & currency found--->');
  
      if (!companyTG.default_price_list_id) throw new Error('Price list is missing');
      if (!companyTG.company_code) throw new Error('Company Code is missing');

      const details = economic.getDetails(addresses, companyTG, currency, productDetails);
      const customer = await economic.findOrCreateCustomer(details, companyTG, addresses[0], axios);
      const invoiceBody =  await economic.getInvoiceTemplate(customer.customerNumber, axios);
      await axios.updateLighthouseEntry({ status: 'ongoing', issue: 'Found/Created Customer' });
      console.log('<----Invoice template found---->')
  
      const products = await tg.getProductsTG(productDetails, axios);
      console.log('<----Products prepared---->');
  
      const draftInvoice = await economic.createDraftInvoice(invoice.invoice, invoiceBody, addresses, products, axios);
  
      console.log('<----Draft Invoice created---->')
      await axios.updateLighthouseEntry({ status: 'ongoing', issue: 'Draft Invoice created' });
  
      const data = await economic.createBookedInvoice(draftInvoice, axios);
  
      console.log('<---Booked invoice created--->', data);
      await axios.updateLighthouseEntry({ status: 'success', issue: 'No Issue' });
  
      return { data };

    } else {
      await axios.updateLighthouseEntry({ status: 'success', issue: 'Not B2B' });
      console.log('Not B2B invoice');
      return { msg: 'Not B2B' };
    }

  } catch (error) {
    const { message, stack, isAxiosError = false } = error;

    if (!isAxiosError && key) { // any synchronous error
      const errorObj = { message, stack, isAxiosError, timestamp: new Date().toISOString() };
      await axios.updateLighthouseEntry({ optional: { error: errorObj } });
    }

    return {
      statusCode: 400,
      body: {
        error: {
          message,
          stack
        }
      }
    };
  }
}
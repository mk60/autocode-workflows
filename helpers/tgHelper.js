const headers = {
    Authorization: `Bearer ${process.env.TG_TOKEN}`,
    'Content-Type': 'application/json'
};

exports.getInvoice = async (id, axios) => {
    const BASE_URL = `https://api.tradegecko.com/invoices/${id}`;
    const { data: invoice } = await axios.get(BASE_URL, { headers });
    return invoice;
}
exports.getAddress = async (ids, axios) => {
    const BASE_URL = `https://api.tradegecko.com/addresses?ids[]=${ids}`;
    const { data : { addresses: addresses } } = await axios.get(BASE_URL, { headers });
    const address = addresses.length > 1 ? addresses : [...addresses, ...addresses]; 
    return address;
};

exports.getCurrency = async (id, axios) => {
    const BASE_URL = `https://api.tradegecko.com/currencies/${id}`;
    const { data: { currency } } = await axios.get(BASE_URL, { headers });
    return currency.iso;
};

exports.getCompany = async (id, axios) => {
    const BASE_URL = `https://api.tradegecko.com/companies/${id}`;
    const { data:  { company: customer } } = await axios.get(BASE_URL, { headers });
    return customer;
};

exports.getOrderLineItems = async (ids, axios) => {
    const BASE_URL = `https://api.tradegecko.com/order_line_items?ids[]=${ids}`;
    const { data: { order_line_items:  lines } } = await axios.get(BASE_URL, { headers });
    return lines;
};

const getVariants = async (ids, axios) => {
    const BASE_URL = `https://api.tradegecko.com/variants?ids[]=${ids}`;
    const { data : { variants: variants } } = await axios.get(BASE_URL, { headers });
    return variants;
};

exports.getProductsTG = async (lineItems, axios) => {
    let variantIds = [], shipping = {};
    
    lineItems.forEach(line => {
        variantIds.push(line.variant_id);
        if (line.label && line.label.trim() === 'Shipping') {
          shipping = line;
        }
    });
    const lineItemsWithoutShipping = lineItems.filter(item => {
      let label = item.label ? item.label.trim() : item.label;
      return label != 'Shipping';
    });

    const variants = await getVariants(variantIds.join('&ids[]='), axios);
    const lineItemsBody = lineItemsWithoutShipping.map(item => {
      if (item.label != 'Shipping') {
        return ({
        "product": {
          },
        "quantity": parseInt(item.quantity),
        "unitNetPrice": parseInt(item.price),
        "discountPercentage": parseInt(item.discount),
      });
      }
    });
    
    
     
      if (variants.length > 0) {
        variants.forEach((variant, index) => {
          if (!lineItemsBody[index].product.productNumber) {
            lineItemsBody[index].product.productNumber = variant.sku;
            lineItemsBody[index].description = variant.product_name;
          }
            
        });
      }
      
      if (Object.keys(shipping).length > 0) {
        const obj = {
          "product": {
            "productNumber": "8"
          },
          "description": "Shipping",
          "quantity": parseInt(shipping.quantity),
          "unitNetPrice": parseInt(shipping.price),
          "discountPercentage": parseInt(shipping.discount),
        }
        lineItemsBody.push(obj);;
      }
    
    return lineItemsBody;
};



const headers = {
  "x-appsecrettoken":  `${process.env.EC_SECRET}`,
  "x-agreementgranttoken": `${process.env.EC_AGREEMENT}`,
  "content-type": "application/json",
};   
let priceLists = {
    'buy': {
      currency: 'DKK',
      customerGroup: 1
    },
    'wholesale': {
      currency: 'DKK',
      customerGroup: 6
    },
    'retail': {
      currency: 'DKK',
      customerGroup: 1
    },
    '40130': {
      currency: 'DKK',
      customerGroup: 1
    },
    '40565': {
      currency: 'DKK',
      customerGroup: 1
    },
    '40572': {
      currency: 'EUR',
      customerGroup: 3
    },
    '40786': {
      currency: 'DKK',
      customerGroup: 7
    }
  };
  
  
  let euCountries = [
    'Austria', 'Belgium', 'Bulgaria',
    'Croatia', 'Cyprus', 'Czech Republic',
    'Estonia', 'Finland', 'France',
    'Germany', 'Greece', 'Hungary',
    'Ireland', 'Italy', 'Latvia',
    'Lithuania', 'Luxembourg', 'Malta',
    'Netherlands', 'Poland', 'Portugal',
    'Romania', 'Slovakia', 'Slovenia',
    'Spain', 'Sweden', 'United Kingdom'
  ];

const getVatZoneNumber = ({ shipping_address_country: country = 'Denamrk'}) => {
    let vatZoneNumber;
    if (country == 'Denmark') { //Domestic
        vatZoneNumber = 1;
      } else if (euCountries.includes(country)) { //EU
        vatZoneNumber = 2;
      } else { //Abroad
        vatZoneNumber = 3;
      }
      return vatZoneNumber;
}

// Check if Order Email is present and assign other email if absent
const getEmail = (companyEmail, addresses) => {
    return companyEmail ? companyEmail : (addresses[0].email ? addresses[0].email : addresses[1].email);
}

// Check if priceList is GBP and add VAT
const getVatPrice = (currency = '', priceListId, priceLines, quantityLines, discountLines) => {
    let vat = 0;
    let priceList = priceLists[priceListId.trim()];
    if (currency === 'GBP') {
        priceList.currency = 'GBP';
        for (let i in priceLines) {
            let quantity = parseFloat(quantityLines[i]);
            let price = parseFloat(priceLines[i]) * quantity;
            let discount = parseFloat(discountLines[i]) / 100 * price;
            vat += parseFloat(((price - discount) * 0.2).toFixed(2)); // round off to 2 decimals
        }
    }
    return vat == 0 ? '' : +vat.toFixed(2);
}

// Get Currency, Customer Group and VAT Zone
exports.getDetails = (addresses, company, currency, productDetails) => {
  let quantity = [],
        price = [],
        discount = [];
    productDetails.forEach(product => {
      quantity.push(product.quantity);
      price.push(product.price);
      discount.push(product.discount);
    }); 

    //const isNumeric = /^\d+$/.test(company.company_code);
    let searchBy = 'corporateIdentificationNumber';
    let customerNumber = company.company_code;
    // if(!isNumeric || customerNumber.length > 9) {
    //   searchBy = 'corporateIdentificationNumber';
    //   customerNumber = '';
    // }
    
    return {
        currency: priceLists[company.default_price_list_id.trim()].currency,
        customerGroup: priceLists[company.default_price_list_id.trim()].customerGroup,
        vatZoneNumber: getVatZoneNumber(addresses[1]),
        email: getEmail(company.email, addresses),
        vat: getVatPrice(currency, company.default_price_list_id, price, quantity, discount),
        searchBy,
        customerNumber
    };
};

const createCustomer = async (headers, body, company, address, axios) => {
  const customerBody =  {
        name: company.name,
        customerGroup: {
          customerGroupNumber: parseInt(body.customerGroup)
        },
        email: body.email,
        address: address.address1,
        city: address.city,
        country: address.country,
        corporateIdentificationNumber: company.company_code, 
        vatZone: {
          vatZoneNumber: parseInt(body.vatZoneNumber)
        },
        currency: body.currency,
        paymentTerms: {
          paymentTermsNumber: 6
        },
        telephoneAndFaxNumber: company.phone_number,
        website: `${company.website}`,
        zip: `${address.zip_code}`
    };  
    const BASE_URL = `https://restapi.e-conomic.com/customers/`;
      const { data:  customer } = await axios.post(BASE_URL, customerBody, { headers });
      console.log('<----Customer created---->');
      return customer;
};

exports.findOrCreateCustomer = async(body, company, address, axios) => {
    const BASE_URL = `https://restapi.e-conomic.com/customers/?filter=${body.searchBy}$eq:${company.company_code}`;   

    const { data: { collection : [ customer ]}} = await axios.get(BASE_URL, { headers });
    if (customer) {
      console.log('<----Customer found---->');
      return customer;
    } else {      
      return createCustomer(headers, body, company, address, axios);
    }
};

exports.getInvoiceTemplate = async (customerNumber, axios) => {
  const { data: invoiceBody }  =  await axios.get(`https://restapi.e-conomic.com/customers/${customerNumber}/templates/invoice`, { headers });
  return invoiceBody;
};

exports.createDraftInvoice = async (invoice, invoiceBody, addresses, products, axios) => {
  invoiceBody.delivery = {};
  invoiceBody.references = {};
  invoiceBody.delivery.address = addresses[1].address1 + addresses[1].address2;
  invoiceBody.delivery.city = addresses[1].city;
  invoiceBody.delivery.zip = addresses[1].zip_code;
  invoiceBody.delivery.country = addresses[1].country;
  invoiceBody.lines = products;
  invoiceBody.references.other = invoice.order_number;
  const { data: draftInvoice  } = await axios.post('https://restapi.e-conomic.com/invoices/drafts', invoiceBody, 
  { headers });
  return draftInvoice;
};

exports.createBookedInvoice = async (draftInvoices, axios) => {
  const bookedInvoice = {
    draftInvoice: {
      draftInvoiceNumber: parseInt(draftInvoices.draftInvoiceNumber)
    }
  };
    const { data } = await axios.post('https://restapi.e-conomic.com/invoices/booked', bookedInvoice, {
      headers });
  return data;
};